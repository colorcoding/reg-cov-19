### Open python file to see the results

# Regression-covid-19
The purpose of this exercise is to predict the number of covid-19 cases in India using Non-linear regression based on data available at https://data.humdata.org/dataset/novel-coronavirus-2019-ncov-cases#metadata-0 

### Covid-19 data obtained from https://data.humdata.org/dataset/novel-coronavirus-2019-ncov-cases#metadata-0

CSV link is
https://data.humdata.org/hxlproxy/api/data-preview.csv?url=https%3A%2F%2Fraw.githubusercontent.com%2FCSSEGISandData%2FCOVID-19%2Fmaster%2Fcsse_covid_19_data%2Fcsse_covid_19_time_series%2Ftime_series_covid19_confirmed_global.csv&filename=time_series_covid19_confirmed_global.csv 
The csv should be downloaded locally to be used in the code as the direct use of csv gives error - > urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed

### Pre-requisites: 
Python, 
jupyter-notebook,
Data analysis libraries - pandas, numpy, scipy, matplotlib, plotly, sklearn,

